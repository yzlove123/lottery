# lottery

#### 介绍
java抽奖程序[分布式]

#### 软件架构
spring-boot + redis + mysql + lua分布式抽奖程序
设置1-5个等级的奖品，可供用户抽奖，并且支持多种抽奖规则:
可根据需要随意配置：
1. 1等奖只能抽一次
2. X等奖3天内只能出现一次
3. 同一用户，N天内不能再抽X奖
4. X奖品每天最多抽出N个
5. 抽奖的用户不能再抽中

关于加锁：
此代码是分布式抽奖，实际运用到本人的公司抽奖活动中
如果有需要改成单应用无redis抽奖，可联系本人微信：LL18835792273

#### 安装教程

1. 需要redis[若不支持，可以联系本人]
2. mysql
3. jdk1.8

#### 使用说明

1. 导入test.json到postman软件中即可使用

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)