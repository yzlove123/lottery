package com.example.lottery.module.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.lottery.module.entity.UserInvestRecord;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * 奖品池表
 *
 * @author liulei, lei.liu@htouhui.com
 * @version 1.0
 */
@Mapper
@Component
public interface UserInvestRecordMapper extends BaseMapper<UserInvestRecord> {

}