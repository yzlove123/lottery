package com.example.lottery.module.entity;

import com.example.lottery.module.enums.PoolTypeEnum;
import com.example.lottery.module.enums.PrizeLevelEnum;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author liulei , lei.liu@htouhui.com
 * @version 1.0.0
 * @date 2019-09-20
 */
@Data
@Accessors(chain = true)
public class PrizePickRecord implements Serializable {
    private Integer id;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 奖品编号
     */
    private String prizeCode;

    /**
     * 奖品类型
     */
    private PoolTypeEnum poolType;

    /**
     * 创建时间
     */
    private Date createTime;

    @Override
    public String toString() {
        return "PrizePickRecord{" +
                "id=" + id +
                ", userId='" + userId + '\'' +
                ", prizeCode='" + prizeCode + '\'' +
                ", poolType=" + poolType +
                '}';
    }
}