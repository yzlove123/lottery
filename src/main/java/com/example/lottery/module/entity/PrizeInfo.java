package com.example.lottery.module.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author liulei , lei.liu@htouhui.com
 * @version 1.0.0
 * @date 2019-09-20
 */
@Data
public class PrizeInfo implements Serializable {
    private Integer id;

    /**
     * 奖品编号
     */
    private String code;

    /**
     * 奖品名称
     */
    private String name;

    /**
     * 奖品来源
     */
    private String source;

    /**
     * 市场价
     */
    private Long price;

    /**
     * 中奖概率
     */
    private BigDecimal rate;

    /**
     * 奖品介绍
     */
    private String description;

    /**
     * 创建时间
     */
    private Date createTime;

}