package com.example.lottery.module.entity;

import com.example.lottery.module.enums.PrizeLevelEnum;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author liulei , lei.liu@htouhui.com
 * @version 1.0.0
 * @date 2019-09-20
 */
@Data
public class PrizeLevelInvest implements Serializable {
    private Integer id;

    /**
     * 奖品级别
     */
    private PrizeLevelEnum level;

    /**
     * 投资额度
     */
    private BigDecimal invest;

    /**
     * 描述
     */
    private String description;

    @Override
    public String toString() {
        return "PrizeLevelInvest{" +
                ", level='" + level + '\'' +
                ", invest=" + invest +
                ", description='" + description + '\'' +
                '}';
    }
}