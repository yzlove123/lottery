package com.example.lottery.module.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author liulei, lei.liu@htouhui.com
 * @version 1.0
 */
@Data
@Accessors(chain = true)
public class PoolType {

    private Integer id;
    private String name;
    private Date startDate;
    private Date endDate;

}
