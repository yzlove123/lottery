package com.example.lottery.module.entity;

import com.baomidou.mybatisplus.annotation.Version;
import com.example.lottery.module.enums.PoolTypeEnum;
import com.example.lottery.module.enums.PrizeLevelEnum;
import com.example.lottery.module.enums.PrizePoolStatusEnum;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author liulei , lei.liu@htouhui.com
 * @version 1.0.0
 * @date 2019-09-20
 */
@Data
@Accessors(chain = true)
public class PrizePool implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 1-中秋抽奖池 2-国庆奖品池 3-元旦抽奖池 4-欢闹春节抽奖池
     */
    private PoolTypeEnum poolType;

    /**
     * 奖品编号
     */
    private String prizeCode;

    /**
     * 1-钻石礼盒 2-铂金礼盒 3-黄金礼盒 4-热情礼盒
     */
    private PrizeLevelEnum level;

    /**
     * 库存
     */
    private Integer inventory;

    /**
     * 抽中限制[单个用户]
     */
    private Integer once;

    /**
     * 每天的抽中次数限制[针对所有用户]
     */
    private Integer dayTimes;

    /**
     * N天内不允许再次抽中[针对所有用户]
     */
    private Integer limitDays;

    /**
     * 1-启用 0-停用
     */
    private PrizePoolStatusEnum status;

    /**
     * 开始时间
     */
    private Date startTime;

    /**
     * 结束时间
     */
    private Date endTime;

    /**
     * 版本号
     */
    @Version
    private Integer version;

}