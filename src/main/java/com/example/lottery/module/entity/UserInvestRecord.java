package com.example.lottery.module.entity;

import com.example.lottery.module.enums.InvestStatusEnum;
import com.example.lottery.module.enums.InvestTypeEnum;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author liulei , lei.liu@htouhui.com
 * @version 1.0.0
 * @date 2019-09-20
 */
@Data
public class UserInvestRecord implements Serializable {
    private Integer id;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 投资额
     */
    private BigDecimal invest;

    /**
     * 投资类型
     */
    private InvestTypeEnum type;

    /**
     * 0-无效 1-有效
     */
    private InvestStatusEnum status;

    private Date createTime;
}