package com.example.lottery.module.exception;

/**
 * @author liulei, lei.liu@htouhui.com
 * @version 1.0
 */
public class LotteryException extends BaseException {

    /**
     * 抽奖池暂无奖品
     */
    public static final Integer PRIZE_POOL_EMPTY = 100;
    /**
     * 抽奖池暂无奖品
     */
    public static final Integer DATE_TIME_FORMAT_ERROR = 800;
    /**
     * Redis加锁失败
     */
    public static final Integer LOCK_REDIS_KEY_FAIL = 900;

    /**
     * 库存减少失败
     */
    public static final Integer INVENTORY_SUBTRACT_FAIL = 1000;

    /**
     * 库存不足
     */
    public static final Integer INVENTORY_IS_POOR = 1100;

    public LotteryException(int code) {
        super(code);
    }

    public LotteryException(int code, String errorMsg) {
        super(code, errorMsg);
    }
}
