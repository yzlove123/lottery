package com.example.lottery.module.exception;

/**
 * 基础错误异常
 *
 * @author liulei, lei.liu@htouhui.com
 * @version 1.0
 */
public class BaseException extends Exception {

    public static final int ERROR_CODE_ILLEGAL_ARGUMENTS = 600;
    public static final int ERROR_CODE_NO_AUTHENTICATED = 601;
    public static final int ERROR_CODE_MISSING_DIGEST = 602;
    public static final int ERROR_CODE_WRONG_DIGEST = 603;
    public static final int ERROR_CODE_UNAUTHORIZED = 718;
    /**
     * 错误码
     */
    private int code;
    /**
     * 错误信息
     */
    private String errorMessage;

    public BaseException(int code) {
        super("error code " + code);
        this.code = code;
    }

    public BaseException(int code, String errorMessage) {
        super(errorMessage);
        this.code = code;
        this.errorMessage = errorMessage;
    }

    public int getCode() {
        return code;
    }


    public String getErrorMessage() {
        return errorMessage;
    }
}
