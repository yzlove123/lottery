package com.example.lottery.module.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Objects;

/**
 * 奖品级别枚举
 *
 * @author liulei, lei.liu@htouhui.com
 * @version 1.0
 */
public enum PrizeLevelEnum implements IEnum<Integer> {

    /**
     * 对应erp_prize_level_invest表level字段
     */
    ONE(1, "一等奖"),
    TWO(2, "二等奖"),
    THIRD(3, "三等奖"),
    FOUR(4, "四等奖"),
    THANKS(5, "参与奖");

    @EnumValue
    private Integer value;
    private String desc;

    PrizeLevelEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    @JsonValue
    @Override
    public Integer getValue() {
        return value;
    }

    public static PrizeLevelEnum valueOf(Integer code) {
        for (PrizeLevelEnum value : PrizeLevelEnum.values()) {
            if (Objects.equals(value.getValue(), code)) {
                return value;
            }
        }
        throw new RuntimeException("无匹配的[" + code + "]");
    }
}
