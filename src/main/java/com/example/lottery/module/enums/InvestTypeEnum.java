package com.example.lottery.module.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Objects;

/**
 * 投资类型
 *
 * @author liulei, lei.lui@htouuhi.com
 * @version 1.0
 */
public enum  InvestTypeEnum implements IEnum<Integer> {

    /**
     * 投资类型
     */
    DAILY(0,"日常投资"),

    ACTIVITY(1,"活动期间投资");

    InvestTypeEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    @EnumValue
    private Integer value;

    private String desc;

    @Override
    @JsonValue
    public Integer getValue() {
        return value;
    }

    public static InvestTypeEnum valueOf(Integer code) {
        for (InvestTypeEnum value : InvestTypeEnum.values()) {
            if (Objects.equals(value.getValue(), code)) {
                return value;
            }
        }
        throw new RuntimeException("无匹配的[" + code + "]");
    }
}
