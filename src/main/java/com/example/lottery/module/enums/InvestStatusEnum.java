package com.example.lottery.module.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Objects;

/**
 * 投资状态类型
 *
 * @author liulei, lei.lui@htouuhi.com
 * @version 1.0
 */
public enum InvestStatusEnum implements IEnum<Integer> {

    /**
     * 投资类型
     */
    ON(0, "正在匹配标"),

    SUCCESS(1, "投资成功"),

    FAIL(2, "投资失败");

    InvestStatusEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    @EnumValue
    private Integer value;

    private String desc;

    @Override
    @JsonValue
    public Integer getValue() {
        return value;
    }

    public static InvestStatusEnum valueOf(Integer code) {
        for (InvestStatusEnum value : InvestStatusEnum.values()) {
            if (Objects.equals(value.getValue(), code)) {
                return value;
            }
        }
        throw new RuntimeException("无匹配的[" + code + "]");
    }
}
