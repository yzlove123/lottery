package com.example.lottery.module.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Objects;

/**
 * 抽奖池类型
 *
 * @author liuelei, lei.liu@htouhui.com
 * @version 1.0
 */
public enum PoolTypeEnum implements IEnum<Integer> {
    /**
     * 1-中秋抽奖池 2-国庆奖品池 3-元旦抽奖池 4-欢闹春节抽奖池
     */
    MID_AUTUMN(1, "中秋节抽奖池"),

    NATION_DAY(2, "国庆节抽奖池"),

    NEW_YEARS_DAY(3, "元旦抽奖池"),

    SPRING_FESTIVAL(4, "欢闹春节抽奖池");

    @EnumValue
    private Integer value;
    private String desc;

    PoolTypeEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    @JsonValue
    @Override
    public Integer getValue() {
        return value;
    }

    public static PoolTypeEnum valueOf(Integer code) {
        for (PoolTypeEnum value : PoolTypeEnum.values()) {
            if (Objects.equals(value.getValue(), code)) {
                return value;
            }
        }
        throw new RuntimeException("无匹配的[" + code + "]");
    }
}
