package com.example.lottery.module.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Objects;

/**
 * 投资状态类型
 *
 * @author liulei, lei.lui@htouuhi.com
 * @version 1.0
 */
public enum PrizePoolStatusEnum implements IEnum<Integer> {

    /**
     * 投资类型
     */
    VALID(1, "启用"),

    NON_VALID(0, "停用");

    PrizePoolStatusEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    @EnumValue
    private Integer value;

    private String desc;

    @Override
    @JsonValue
    public Integer getValue() {
        return value;
    }

    public static PrizePoolStatusEnum valueOf(Integer code) {
        for (PrizePoolStatusEnum value : PrizePoolStatusEnum.values()) {
            if (Objects.equals(value.getValue(), code)) {
                return value;
            }
        }
        throw new RuntimeException("无匹配的[" + code + "]");
    }
}
