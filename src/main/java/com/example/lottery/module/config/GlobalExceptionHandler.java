package com.example.lottery.module.config;

import com.example.lottery.module.entity.JsonResult;
import com.example.lottery.module.exception.BaseException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * 全局异常处理器
 *
 * @author liulei, lei.liu@htouhui.com
 * @version 1.0
 */
@ControllerAdvice
@ResponseBody
@Slf4j
public class GlobalExceptionHandler {

    @Autowired
    private MessageSource messageSource;

    /**
     * 一般异常处理
     */
    @ExceptionHandler(Exception.class)
    public JsonResult defaultErrorHandler(HttpServletRequest request, Exception e, Locale locale) {
        JsonResult result = new JsonResult();
        int code = 500;
        String errorMessage = null;
        Throwable exception = e;

        if (e instanceof UndeclaredThrowableException) {
            exception = ((UndeclaredThrowableException) e).getUndeclaredThrowable();
            code = ((BaseException) exception).getCode();
            errorMessage = ((BaseException) exception).getErrorMessage();
        } else if (exception instanceof BaseException) {
            code = ((BaseException) exception).getCode();
            errorMessage = ((BaseException) exception).getErrorMessage();
        } else if (exception instanceof MissingServletRequestParameterException) {
            code = BaseException.ERROR_CODE_ILLEGAL_ARGUMENTS;
            errorMessage = exception.getMessage();
        } else {
            log.error("Unknown Exception, URI = " + request.getRequestURI(), e);
        }
        // 如果异常里已经包含了错误信息，则不会再通过错误码获取预先定义的错误信息
        if (StringUtils.isEmpty(errorMessage)) {
            String prefix = exception.getClass().getSimpleName();
            errorMessage = getMessage(prefix + "." + code, locale);
            if (errorMessage == null) {
                errorMessage = getMessage(Integer.toString(code), locale);
            }
        }
        result.setStatus(code);
        result.setMessage(errorMessage);
        log.error("URI = {}, errorCode = {}, errorMessage = {}", request.getRequestURI(), code, errorMessage);
        return result;
    }

    private String getMessage(String key, Locale locale) {
        String errorMessage = null;
        try {
            errorMessage = messageSource.getMessage(key, null, locale);
        } catch (NoSuchMessageException exception) {
            log.error("ErrorMessage|NotFound|Custom|Exception|Msg|{}", key);
        }
        return errorMessage;
    }

    /**
     * JSR303验证规范的@Valid实体类注解校验异常处理
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public JsonResult argumentDetailValidate(MethodArgumentNotValidException ex) {
        BindingResult bindingResult = ex.getBindingResult();
        Map<String, String> errorsMap = getErrorsMap(bindingResult);
        String errorMsg = String.join(";", errorsMap.values());
        JsonResult jsonResult = new JsonResult();
        jsonResult.setStatus(BaseException.ERROR_CODE_ILLEGAL_ARGUMENTS);
        jsonResult.setMessage(errorMsg);
        return jsonResult;
    }

    private Map<String, String> getErrorsMap(BindingResult result) {
        List<FieldError> fieldErrors = result.getFieldErrors();

        Map<String, String> errorsMap = new HashMap<>(10);
        for (FieldError fieldError : fieldErrors) {
            String localizedErrorMessage = fieldError.getDefaultMessage();
            if (StringUtils.isNotBlank(localizedErrorMessage)) {
                errorsMap.put(fieldError.getField(), localizedErrorMessage);
            }
        }
        return errorsMap;
    }

    /**
     * 参数缺失异常
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public JsonResult argumentFormatValidate(HttpMessageNotReadableException ex) {
        JsonResult jsonResult = new JsonResult();
        jsonResult.setStatus(BaseException.ERROR_CODE_ILLEGAL_ARGUMENTS);
        jsonResult.setMessage("方法参数缺失/格式不匹配");
        return jsonResult;
    }

}
