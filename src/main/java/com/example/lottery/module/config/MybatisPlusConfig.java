package com.example.lottery.module.config;

import com.baomidou.mybatisplus.extension.plugins.OptimisticLockerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author liulei, lei.liu@htouhui.com
 * @version 1.0
 */
@EnableTransactionManagement
@Configuration
public class MybatisPlusConfig {

    /**
     * 乐观锁 插件
     */
    @Bean
    public OptimisticLockerInterceptor optimisticLocker() {
        return new OptimisticLockerInterceptor();
    }
}
