package com.example.lottery.module.config.redis;

import com.example.lottery.module.exception.LotteryException;

/**
 * @FunctionalInterface SAM接口[编译检查]
 * @author liulei, lei.liu@htouhui.com
 * @version 1.0
 */
@FunctionalInterface
public interface RedisCallBack {

    /**
     * 加锁成功回调
     *
     * @param lock       加锁结果
     * @param expireDate 过期时间
     * @throws LotteryException 异常
     */
    void lockCallback(boolean lock, Long expireDate) throws LotteryException;

}
