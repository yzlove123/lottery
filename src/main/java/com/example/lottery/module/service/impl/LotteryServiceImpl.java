package com.example.lottery.module.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.lottery.module.dto.PrizePoolDto;
import com.example.lottery.module.dto.UserLotteryDto;
import com.example.lottery.module.entity.*;
import com.example.lottery.module.enums.InvestStatusEnum;
import com.example.lottery.module.enums.PoolTypeEnum;
import com.example.lottery.module.enums.PrizePoolStatusEnum;
import com.example.lottery.module.exception.LotteryException;
import com.example.lottery.module.mapper.*;
import com.example.lottery.module.service.LotteryService;
import com.example.lottery.module.utils.DataUtils;
import com.example.lottery.module.utils.DateUtils;
import com.example.lottery.module.utils.RedisUtils;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.example.lottery.module.exception.LotteryException.*;

/**
 * 抽奖服务类
 *
 * @author liulei, lei.liu@htouhui.com
 * @version 1.0
 */
@Slf4j
@Service
public class LotteryServiceImpl implements LotteryService {
    /**
     * 虚拟奖品[未抽中]
     */
    private static final String NON_PRIZE = "000";
    /**
     * redis缓存抽奖池前缀
     */
    private static final String PRIZE_POOL_KEY = "PRIZE::POOL::TYPE::";
    private static final String PRIZE_INFO_KEY = "PRIZE::TYPE::TYPE::";

    @Autowired
    private PrizeInfoMapper prizeInfoMapper;

    @Autowired
    private PrizeLevelInvestMapper prizeLevelInvestMapper;

    @Autowired
    private PrizePickRecordMapper prizePickRecordMapper;

    @Autowired
    private UserInvestRecordMapper userInvestRecordMapper;

    @Autowired
    private PrizePoolMapper prizePoolMapper;

    @Autowired
    private PoolTypeMapper poolTypeMapper;

    @Autowired
    private RedisUtils redisUtils;

    @Override
    public List<PrizeInfo> getPrizeInfo() {
        List<PrizeInfo> prizeInfos = (List<PrizeInfo>) redisUtils.get(PRIZE_INFO_KEY + "ALL");
        if (!CollectionUtils.isEmpty(prizeInfos)) {
            log.info("GET|PRIZE|BASE|INFO|FROM|REDIS|CACHE|{}", prizeInfos);
            return prizeInfos;
        }
        prizeInfos = this.prizeInfoMapper.selectList(new LambdaQueryWrapper<>());
        log.info("SET|PRIZE|BASE|INFO|TO|REDIS|CACHE|{}", prizeInfos);
        redisUtils.set(PRIZE_INFO_KEY + "ALL", prizeInfos, 7, TimeUnit.DAYS);
        return prizeInfos;
    }

    @Override
    public void clearPrizeInfoCache() {
        log.info("DELETE|PRIZE|INFO|KEY");
        redisUtils.del(PRIZE_INFO_KEY + "ALL");
    }

    @Override
    public BigDecimal getUserInvestAmount(String userId, Integer poolType) {
        Preconditions.checkNotNull(userId, "用户Id不能为空", userId);
        PoolType type = this.poolTypeMapper.selectById(poolType);
        log.info("GET|USER|INVEST|AMOUNT|DURING|ACTIVITY|BY|USER|ID|DATE|{}|{}|{}", userId, type.getStartDate(), type.getEndDate());
        BigDecimal investAmount = this.userInvestRecordMapper.selectList(new LambdaQueryWrapper<UserInvestRecord>()
                .eq(UserInvestRecord::getUserId, userId)
                .gt(UserInvestRecord::getCreateTime, type.getStartDate())
                .lt(UserInvestRecord::getCreateTime, type.getEndDate())
                .eq(UserInvestRecord::getStatus, InvestStatusEnum.SUCCESS)
        ).stream().map(UserInvestRecord::getInvest).reduce(BigDecimal.ZERO, BigDecimal::add);
        return investAmount;
    }

    @Override
    public UserLotteryDto getUserLotteryTimes(UserLotteryDto userLottery) {
        //1.获取投资-抽奖总区间
        List<PrizeLevelInvest> levelInvest = getPrizeLevelInvest();
        //2.计算当前用户的投资额度
        BigDecimal investAmount = this.getUserInvestAmount(userLottery.getUserId(), userLottery.getPoolType());
        log.info("USER|{}|INVEST|DURING|ACTIVITY|{}", userLottery.getUserId(), investAmount);
        //3.适配投资额区间计算出理论上存在多少次抽奖
        int lotteryTimes = DataUtils.getLotteryTimes(levelInvest, investAmount);
        log.info("USER|{}|HAVE|{}|TIMES|TO|LOTTERY|DURING|ACTIVITY", userLottery.getUserId(), lotteryTimes);
        //4.查看抽奖记录[活动期间]
        List<PrizePickRecord> pickRecords = this.prizePickRecordMapper.selectList(new LambdaQueryWrapper<PrizePickRecord>().ne(PrizePickRecord::getPrizeCode, NON_PRIZE).eq(PrizePickRecord::getUserId, userLottery.getUserId()));
        int times = lotteryTimes - pickRecords.size();
        assert times > -1;
        log.info("USER|{}|HAD|LOTTERY|{}|TIMES|DURING|ACTIVITY", userLottery.getUserId(), times);
        userLottery.setLotteryTimes(times).setInvestAmount(investAmount);
        return userLottery;
    }

    /**
     * 条件:
     * 1.还有抽奖次数
     * 2.抽奖池目前的奖品数 > 0
     * 3.奖品的限制条件没有触发
     * 依次对数据进行过滤处理[打怪模式]
     */
    @Override
    public List<PrizePoolDto> getUserPrizePool(UserLotteryDto userLottery) {
        //1.抽奖次数判断
        String userId = userLottery.getUserId();
        userLottery = this.getUserLotteryTimes(userLottery);
        int lotteryTimes = userLottery.getLotteryTimes();
        if (lotteryTimes <= 0) {
            return null;
        }
        log.info("USER|{}|HAVE|{}|TIMES|TO|LOTTERY|DURING|ACTIVITY", userId, lotteryTimes);
        List<PrizeLevelInvest> levelInvest = getPrizeLevelInvest();
        //2.用户可抽的抽奖池奖品数>0的奖品池[前提:活动期间投资总额达到对应可抽的标准]
        BigDecimal investAmount = userLottery.getInvestAmount();
        assert investAmount != null;
        PoolTypeEnum poolTypeEnum = PoolTypeEnum.valueOf(userLottery.getPoolType());
        List<PrizePoolDto> pool = this.getPrizePool(poolTypeEnum);
        List<PrizePoolDto> userPrizePool = DataUtils.userPrizePool(pool, levelInvest, investAmount);
        log.info("USER|{}|CAN|LOTTERY|PRIZE|{}|DURING|ACTIVITY", userId, userPrizePool);
        //3.对存在限制的奖品进行剔除[1-找出每个奖品的限制条件 2-所有用户的抽奖记录是否存在这种限制条件]
        PoolType poolType = this.poolTypeMapper.selectById(poolTypeEnum.getValue());
        List<PrizePickRecord> recordList = this.prizePickRecordMapper.selectList(new LambdaQueryWrapper<PrizePickRecord>().ne(PrizePickRecord::getPrizeCode, NON_PRIZE).gt(PrizePickRecord::getCreateTime, poolType.getStartDate()).lt(PrizePickRecord::getCreateTime, poolType.getEndDate()));
        final LocalDateTime currentLocalDateTime = DateUtils.getCurrentLocalDateTime();
        // 对于某些贵重奖品，一次活动只能出现若干次的时候，利用库存进行控制
        userPrizePool = userPrizePool.stream().filter(prizePool -> {
            String prizeCode = prizePool.getPrizeCode();
            // 限制条件1:库存>0
            Integer inventory = prizePool.getInventory();
            if (inventory == 0) {
                log.error("=======USER|{}|SKIP|PRIZE|POOL|OF|LIMIT|奖品[{}]库存不足", userId, prizeCode);
                return false;
            }
            // 限制条件2:N天不允许再次抽中[所有用户]
            Integer limitDays = prizePool.getLimitDays();
            LocalDateTime prizeLatestLotteryDate = DataUtils.getPrizeLatestLotteryDate(recordList, prizeCode);
            if (null != prizeLatestLotteryDate && DateUtils.betweenTwoTime(prizeLatestLotteryDate, currentLocalDateTime, ChronoUnit.DAYS) < limitDays) {
                log.error("=======USER|{}|SKIP|PRIZE|POOL|OF|LIMIT|{}天内不允许再次抽中奖品[{}]", userId, limitDays, prizeCode);
                return false;
            }
            // 限制条件3:允许每天的抽中次数[所有用户]
            Integer dayTimes = prizePool.getDayTimes();
            int lotteryDatTimes = DataUtils.getPrizeLotteryDatTimes(recordList, prizeCode, currentLocalDateTime);
            if (lotteryDatTimes >= dayTimes) {
                log.error("=======USER|{}|SKIP|PRIZE|POOL|OF|LIMIT|每天只允许抽中奖品[{}]{}次", userId, prizeCode, dayTimes);
                return false;
            }
            // 限制条件4:每个用户只允许抽中1次[单个用户]
            Integer once = prizePool.getOnce();
            if (null != once && DataUtils.getUserLotteryPrizeTimes(recordList, prizeCode, userId) > 0) {
                log.error("=======USER|{}|SKIP|PRIZE|POOL|OF|LIMIT|每个用户只允许抽中奖品[{}]{}次", userId, prizeCode, once);
                return false;
            }
            return true;
        }).collect(Collectors.toList());
        List<PrizeInfo> prizeInfo = this.getPrizeInfo();
        userPrizePool.forEach(vo -> prizeInfo.forEach(prize -> {
            if (vo.getPrizeCode().equals(prize.getCode())) {
                vo.setRate(prize.getRate());
                return;
            }
        }));
        return userPrizePool;
    }

    @Override
    public List<PrizeLevelInvest> getPrizeLevelInvest() {
        return this.prizeLevelInvestMapper.selectList(new LambdaQueryWrapper<PrizeLevelInvest>().select(PrizeLevelInvest::getInvest, PrizeLevelInvest::getDescription, PrizeLevelInvest::getLevel));
    }

    @Override
    public List<PrizePoolDto> getPrizePool(PoolTypeEnum poolType) {
        List<PrizePool> prizePools = this.prizePoolMapper.selectList(new LambdaQueryWrapper<PrizePool>().gt(PrizePool::getInventory, 0).eq(PrizePool::getPoolType, poolType).eq(PrizePool::getStatus, PrizePoolStatusEnum.VALID));
        List<PrizePoolDto> result = Lists.newArrayList();
        prizePools.forEach(vo -> {
            PrizePoolDto dto = new PrizePoolDto();
            BeanUtils.copyProperties(vo, dto);
            dto.setLevel(vo.getLevel().getValue());
            dto.setPoolType(vo.getPoolType().getValue());
            dto.setStatus(vo.getStatus().getValue());
            result.add(dto);
        });
        return result;
    }

    /**
     * 使用redis+数据库乐观锁-双重锁
     * 为提高程序效率，对锁的级别粒度控制到最细：奖品
     *
     * @param userLottery 抽奖信息
     * @return 抽奖结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public PrizePoolDto lottery(UserLotteryDto userLottery) throws LotteryException {
        List<PrizePoolDto> userPrizePool = this.getUserPrizePool(userLottery);
        if (CollectionUtils.isEmpty(userPrizePool)) {
            log.info("PRIZE|POOL|INVENTORY|IS|POOR|{}|{}", userLottery, userPrizePool);
            throw new LotteryException(INVENTORY_IS_POOR);
        }
        log.info("USER|START|TO|LOTTERY|PRIZE|POOL|OF|{}", userPrizePool);
        //1.抽奖
        List<BigDecimal> originRates = userPrizePool.stream().map(PrizePoolDto::getRate).collect(Collectors.toList());
        int lotteryPrize = DataUtils.lotteryPrize(originRates);
        PrizePoolDto prizePoolDto = userPrizePool.get(lotteryPrize);
        log.info("USER|{}|LOTTERY|PRIZE|{}", userLottery.getUserId(), prizePoolDto);
        String redisKey = PRIZE_POOL_KEY + prizePoolDto.getPrizeCode();
        // 对抽中的商品加锁
        redisUtils.lock(redisKey, 1, (lock, lockValue) -> {
            if (!lock) {
                log.error("REDIS|LOCK|CALLBACK|FAIL|{}|{}", redisKey, lockValue);
                throw new LotteryException(LOCK_REDIS_KEY_FAIL);
            }
            //2.库存-1[乐观锁]
            int updateById = this.prizePoolMapper.updateById(new PrizePool()
                    .setId(prizePoolDto.getId())
                    .setPrizeCode(prizePoolDto.getPrizeCode())
                    .setInventory(prizePoolDto.getInventory() - 1)
                    .setVersion(prizePoolDto.getVersion())
            );
            if (updateById == 0) {
                log.error("REDIS|LOCK|CALLBACK|FAIL|OF|SUBTRACT|INVENTORY|FAIL|{}", prizePoolDto.getPrizeCode());
                throw new LotteryException(INVENTORY_SUBTRACT_FAIL);
            }
            //TODO 需要对抽中的奖品再次做校验！！！否则存在问题哦！
            //3.抽奖记录+1
            this.prizePickRecordMapper.insert(new PrizePickRecord()
                    .setPrizeCode(prizePoolDto.getPrizeCode())
                    .setUserId(userLottery.getUserId())
                    .setPoolType(PoolTypeEnum.valueOf(userLottery.getPoolType()))
            );
            boolean unlock = redisUtils.unlock(redisKey, lockValue + "");
            log.info("UNLOCK|KEY|{}|{}", redisKey, unlock);
        });
        return prizePoolDto;
    }

}
