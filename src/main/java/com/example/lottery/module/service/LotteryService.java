package com.example.lottery.module.service;

import com.example.lottery.module.dto.PrizePoolDto;
import com.example.lottery.module.dto.UserLotteryDto;
import com.example.lottery.module.entity.PrizeInfo;
import com.example.lottery.module.entity.PrizeLevelInvest;
import com.example.lottery.module.enums.PoolTypeEnum;
import com.example.lottery.module.exception.LotteryException;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;

/**
 * 抽奖服务类
 *
 * @author liulei, lei.liu@htouhui.com
 * @version 1.0
 */
public interface LotteryService {

    /**
     * 获取所有抽奖奖品信息
     *
     * @return 奖品基础信息列表
     */
    List<PrizeInfo> getPrizeInfo();

    /**
     * 清除所有抽奖奖品缓存信息
     *
     * @return 奖品基础信息列表
     */
    void clearPrizeInfoCache();

    /**
     * 获取用户活动期间的投资额
     *
     * @param userId   用户ID
     * @param poolType 抽奖池类型
     * @return 投资总额
     */
    BigDecimal getUserInvestAmount(String userId, Integer poolType);

    /**
     * 获取用户可抽奖次数[根据产品的实际活动规则计算]
     *
     * @param userLottery 信息
     * @return 次数
     * @throws ParseException 时间转换异常
     */
    UserLotteryDto getUserLotteryTimes(UserLotteryDto userLottery);

    /**
     * 查询用户可抽的抽奖池
     *
     * @param userLottery 信息
     * @return 可用抽奖池
     */
    List<PrizePoolDto> getUserPrizePool(UserLotteryDto userLottery);

    /**
     * 获取抽奖-投资区间
     *
     * @return 区间
     */
    List<PrizeLevelInvest> getPrizeLevelInvest();

    /**
     * 获取库存大于0且处于激活状态的抽奖池信息
     *
     * @param poolType 抽奖池类型
     * @return 抽奖池列表
     */
    List<PrizePoolDto> getPrizePool(PoolTypeEnum poolType);

    /**
     * 用户抽奖
     * 1.redis+数据库乐观锁双重保障
     * 2.抽中的奖品在抽奖池中库存-1
     * 3.抽奖记录+1
     *
     * @param userLottery 抽奖信息
     * @return 抽奖结果
     * @throws LotteryException 抽奖异常
     */
    PrizePoolDto lottery(UserLotteryDto userLottery) throws LotteryException;

}
