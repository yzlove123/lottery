package com.example.lottery.module.controller;

import com.example.lottery.module.dto.PrizePoolDto;
import com.example.lottery.module.dto.UserLotteryDto;
import com.example.lottery.module.entity.JsonResult;
import com.example.lottery.module.enums.PoolTypeEnum;
import com.example.lottery.module.exception.LotteryException;
import com.example.lottery.module.service.LotteryService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * 抽奖服务接口
 *
 * @author liulei, lei.liu@htouhui.com
 * @version 1.0
 */
@Slf4j
@RequestMapping("/api/lottery")
@RestController
public class LotteryController {

    @Autowired
    private LotteryService service;

    /**
     * 清除奖品redis缓存
     */
    @GetMapping("/prize/info/clear")
    public JsonResult clearPrizeInfoCache() {
        service.clearPrizeInfoCache();
        return JsonResult.ok();
    }

    /**
     * 查询用户活动期间投资额
     */
    @GetMapping("/user/invest/amount")
    public JsonResult getUserInvestAmount(@RequestParam("user_id") String userId, @RequestParam("pool_type") Integer poolType) {
        return JsonResult.ok(service.getUserInvestAmount(userId, poolType));
    }

    /**
     * 返回用户的抽奖次数
     */
    @PostMapping("/user/lottery/times")
    public JsonResult getUserLotteryTimes(@Valid @RequestBody UserLotteryDto userLottery) {
        return JsonResult.ok(service.getUserLotteryTimes(userLottery).getLotteryTimes());
    }

    /**
     * 查询用户可抽的抽奖池
     */
    @PostMapping("/user/prize/pool")
    public JsonResult getUserPrizePool(@Valid @RequestBody UserLotteryDto userLottery) {
        return JsonResult.ok(service.getUserPrizePool(userLottery));
    }

    /**
     * 查询所有抽奖池
     */
    @PostMapping("/prize/pool")
    public JsonResult getPrizePool(@Valid @RequestBody PrizePoolDto pool) {
        return JsonResult.ok(service.getPrizePool(PoolTypeEnum.valueOf(pool.getPoolType())));
    }

    /**
     * 用户抽奖[单线程测试]
     */
    @PostMapping("/user/prize/lottery")
    public JsonResult lottery(@Valid @RequestBody UserLotteryDto userLottery) throws LotteryException {
        int i = 0;
        Map<String, Integer> map = Maps.newHashMap();
        do {
            PrizePoolDto lottery = service.lottery(userLottery);
            String prizeCode = lottery.getPrizeCode();
            map.merge(prizeCode, 1, (a, b) -> a + b);
            i++;
        } while (i < 10);
        log.info("抽奖结果:{}", map.toString());
        return JsonResult.ok();
    }

    /**
     * 用户抽奖[多线程测试]
     */
    @PostMapping("/user/prize/lottery/thread")
    public JsonResult lotteryWithThread() throws LotteryException {
        // 1.构建10000个用户同时测试
        Map<String, Integer> map = Maps.newHashMap();
        PrizePoolDto lottery = service.lottery(new UserLotteryDto());
        String prizeCode = lottery.getPrizeCode();
        map.merge(prizeCode, 1, (a, b) -> a + b);
        log.info("抽奖结果:{}", map.toString());
        return JsonResult.ok();
    }

    public static List<UserLotteryDto> generateUserLotterys() {
        return Lists.newArrayList();
    }

}
