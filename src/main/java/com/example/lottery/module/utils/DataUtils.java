package com.example.lottery.module.utils;

import com.example.lottery.module.dto.PrizePoolDto;
import com.example.lottery.module.entity.PrizeLevelInvest;
import com.example.lottery.module.entity.PrizePickRecord;
import com.example.lottery.module.enums.PrizeLevelEnum;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author liulei, lei.liu@htouhui.com
 * @version 1.0
 */
@Slf4j
public class DataUtils {

    /**
     * 计算用户抽奖次数
     *
     * @param levelInvests 投资-抽奖总区间
     * @param invest       活动期间的投资额
     * @return 抽奖次数
     */
    public static int getLotteryTimes(List<PrizeLevelInvest> levelInvests, BigDecimal invest) {
        assert !levelInvests.isEmpty();
        int levelSize = levelInvests.size();
        int times = 0;
        for (int i = 0; i < levelSize; i++) {
            if (invest.compareTo(levelInvests.get(i).getInvest()) >= 0) {
                times++;
            }
        }
        return times;
    }

    /**
     * 过滤出用户可抽奖的抽奖池奖品类型
     *
     * @param pool         抽奖池奖品
     * @param levelInvests 抽奖-投资区间级别
     * @param investAmount 活动期间用户投资总额
     * @return 抽奖池奖品
     */
    public static List<PrizePoolDto> userPrizePool(List<PrizePoolDto> pool, List<PrizeLevelInvest> levelInvests, BigDecimal investAmount) {
        levelInvests = levelInvests.stream().filter(level -> level.getInvest().compareTo(investAmount) < 0).collect(Collectors.toList());
        List<PrizeLevelEnum> levelEnums = levelInvests.stream().map(PrizeLevelInvest::getLevel).collect(Collectors.toList());
        return pool.stream().filter(prize -> levelEnums.contains(PrizeLevelEnum.valueOf(prize.getLevel()))).collect(Collectors.toList());
    }

    /**
     * 查找奖品最近抽中的时间
     *
     * @param recordsList 所有抽奖记录
     * @param prizeCode   奖品编号
     * @return 奖品最近一次的抽中时间
     */
    public static LocalDateTime getPrizeLatestLotteryDate(List<PrizePickRecord> recordsList, String prizeCode) {
        assert prizeCode != null;
        // 保证抽奖记录时间倒叙排布
        recordsList.sort(Comparator.comparing(PrizePickRecord::getCreateTime).reversed());
        Optional<PrizePickRecord> first = recordsList.stream()
                .filter(record -> prizeCode.equals(record.getPrizeCode()))
                .findFirst();
        if (first.isPresent()) {
            Date pickDate = first.get().getCreateTime();
            assert pickDate != null;
            log.info("PRIZE|{}|LATEST|PICK|DATE|IS|{}", prizeCode, DateUtils.dateConvertToString(pickDate, DateUtils.DATE_TIME_FORMAT));
            return DateUtils.dateConvertToLocalDateTime(pickDate);
        }
        return null;
    }

    /**
     * 查找奖品当天抽中的次数
     *
     * @param recordsList 所有抽奖记录
     * @param prizeCode   奖品编号
     * @param date        当天时间
     * @return 当天抽中次数
     */
    public static int getPrizeLotteryDatTimes(List<PrizePickRecord> recordsList, String prizeCode, LocalDateTime date) {
        assert date != null;
        assert prizeCode != null;

        LocalDateTime toDayStart = DateUtils.getDayStart(date);
        LocalDateTime todayDayEnd = DateUtils.getDayEnd(date);
        return recordsList.stream()
                .filter(record -> prizeCode.equals(record.getPrizeCode()))
                .filter(record -> DateUtils.dateConvertToLocalDateTime(record.getCreateTime()).isAfter(toDayStart))
                .filter(record -> DateUtils.dateConvertToLocalDateTime(record.getCreateTime()).isBefore(todayDayEnd))
                .collect(Collectors.toList())
                .size();
    }

    /**
     * 判断用户抽中过此次奖品次数
     *
     * @param recordsList 所有抽奖记录
     * @param prizeCode   奖品编号
     * @param userId      用户ID
     * @return 用户抽中奖品的次数
     */
    public static int getUserLotteryPrizeTimes(List<PrizePickRecord> recordsList, String prizeCode, String userId) {
        assert userId != null;
        assert prizeCode != null;
        List<PrizePickRecord> pickRecords = recordsList.stream()
                .filter(record -> userId.equals(record.getUserId()) && prizeCode.equals(record.getPrizeCode()))
                .collect(Collectors.toList());
        if (!pickRecords.isEmpty()) {
            log.info("USER|{}|PICK|RECORDS|{}", userId, pickRecords);
        }
        return pickRecords.size();
    }

    /**
     * 抽奖算法[离散法获取奖品下标]
     *
     * @param originRates 原始的概率列表，保证顺序和实际物品对应
     * @return 物品的索引
     */
    public static int lotteryPrize(List<BigDecimal> originRates) {
        // 计算总概率，这样可以保证不一定总概率是1
        BigDecimal sumRate = BigDecimal.ZERO;
        for (BigDecimal rate : originRates) {
            sumRate = sumRate.add(rate);
        }
        // 计算每个物品在总概率的基础下的概率情况，并且形成离散区间
        List<BigDecimal> sortOriginRates = Lists.newArrayListWithCapacity(originRates.size());
        BigDecimal tempSumRate = BigDecimal.ZERO;
        for (BigDecimal rate : originRates) {
            tempSumRate = tempSumRate.add(rate);
            sortOriginRates.add(tempSumRate.divide(sumRate, 20, BigDecimal.ROUND_DOWN));
        }
        // 根据区块值命中离散区间，找到对应的索引下标
        BigDecimal index = new BigDecimal(Math.random());
        sortOriginRates.add(index);
        Collections.sort(sortOriginRates);
        return sortOriginRates.indexOf(index);
    }
}
