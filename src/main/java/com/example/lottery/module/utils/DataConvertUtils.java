package com.example.lottery.module.utils;

import java.util.Collection;
import java.util.function.Supplier;

/**
 * @author liulei, lei.liu@htouhui.com
 * @version 1.0
 */
public class DataConvertUtils {

    /**
     * 集合转换
     *
     * @param sourceCollection  原集合
     * @param collectionFactory 目标集合
     * @param <T>               原集合泛型对象
     * @param <SOURCE>          原集合类型
     * @param <DEST>            目标集合类型
     * @return 目标集合
     * @apiNote Set<Person> rosterSet = transferElements(roster, HashSet::new);
     * @apiNote Set<Person> rosterSet = transferElements(roster, HashSet<Person>::new);
     * @apiNote Set<Person> rosterSetLambda = transferElements(roster, () -> { return new HashSet<>(); });
     */
    public static <T, SOURCE extends Collection<T>, DEST extends Collection<T>> DEST collectionConvert(SOURCE sourceCollection, Supplier<DEST> collectionFactory) {
        DEST result = collectionFactory.get();
        assert sourceCollection != null;
        result.addAll(sourceCollection);
        return result;
    }

}

