package com.example.lottery.module.interceptors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;

/**
 * 处理跨域请求的拦截器，参考（http://www.ruanyifeng.com/blog/2016/04/cors.html）
 *
 * @author redstarstar, star.hong@gmail.com
 * @version 1.0
 */
@Component
public class CorsInterceptor extends HandlerInterceptorAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(CorsInterceptor.class);

    private static final String HEADER_ORIGIN = "Origin";
    private static final String HEADER_ACCESS_CONTROL_REQUEST_METHOD = "Access-Control-Request-Method";
    private static final String HEADER_ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
    private static final String HEADER_ACCESS_CONTROL_ALLOW_CREDENTIALS = "Access-Control-Allow-Credentials";
    private static final String HEADER_ACCESS_CONTROL_ALLOW_METHODS = "Access-Control-Allow-Methods";
    private static final String HEADER_ACCESS_CONTROL_MAX_AGE = "Access-Control-Max-Age";
    private static final String HEADER_ACCESS_CONTROL_ALLOW_HEADERS = "Access-Control-Allow-Headers";

    @Value("${cors.allow-origins}")
    private String allowOrigins;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String origin = request.getHeader(HEADER_ORIGIN);
        LOGGER.debug("CORS {} HEADER ORIGIN {}", request.getMethod(), origin);
        // 不认为是跨域请求，不做任何处理直接执行后续
        if (StringUtils.isBlank(origin)) {
            LOGGER.debug("CORS PASS {}", request.getRequestURI());
            return true;
        }

        boolean isAllowOrigin = false;
        LOGGER.debug("CORS allowOrigins = {}", allowOrigins);
        // 允许任何跨域访问
        if ("*".equals(allowOrigins)) {
            isAllowOrigin = true;
        }
        else {
            String[] allowOriginArray = allowOrigins.split(",");
            for (String allowOrigin : allowOriginArray) {
                if (allowOrigin.equals(origin)) {
                    isAllowOrigin = true;
                    break;
                }
            }
        }
        // 不允许的跨域访问，直接返回false，忽略掉后面的业务逻辑处理
        if (!isAllowOrigin) {
            LOGGER.debug("CORS OriginNotAllowed {} {}", origin, request.getRequestURI());
            return false;
        }

        String requestMethod = request.getMethod();
        // 处理"非简单请求"的"预检请求"
        if (StringUtils.equalsIgnoreCase(requestMethod, "OPTIONS")) {
            String accessControlRequestMethod = request.getHeader(HEADER_ACCESS_CONTROL_REQUEST_METHOD);
            if (!StringUtils.contains("GET, POST, DELETE, PUT", accessControlRequestMethod.toUpperCase())) {
                LOGGER.debug("CORS PreCheck MethodNotAllowed {} {}", accessControlRequestMethod, request.getRequestURI());
                return false;
            }
            response.addHeader(HEADER_ACCESS_CONTROL_ALLOW_ORIGIN, origin);
            // 允许发送Cookie
            response.addHeader(HEADER_ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");
            // 服务器支持的所有跨域请求的方法
            response.addHeader(HEADER_ACCESS_CONTROL_ALLOW_METHODS, "GET, POST, DELETE, PUT");
            // 指定服务器支持的所有头信息字段
            response.addHeader(HEADER_ACCESS_CONTROL_ALLOW_HEADERS, "Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, COOKIES, responseType");
            // 指定本次预检请求的有效期，5分钟
            response.addHeader(HEADER_ACCESS_CONTROL_MAX_AGE, "300");
            LOGGER.debug("CORS PreCheck Allowed {} {}", origin, request.getRequestURI());
            return false;
        }

        response.addHeader(HEADER_ACCESS_CONTROL_ALLOW_ORIGIN, origin);
        // 允许发送Cookie
        response.addHeader(HEADER_ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");
        Enumeration em = request.getParameterNames();

        LOGGER.debug("CORS Allowed {} {}", origin, request.getRequestURI());
        return true;
    }
}
