package com.example.lottery.module.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author liulei, lei.liu@htouhui.com
 * @version 1.0
 */
@Data
@Accessors(chain = true)
public class UserLotteryDto {

    /**
     * 用户Id
     */
    @NotNull(message = "用户ID不能为空")
    private String userId;

    /**
     * 投资总额
     */
    private BigDecimal investAmount;

    /**
     * 抽奖次数
     */
    private Integer lotteryTimes;

    /**
     * 抽奖池类型
     */
    @NotNull(message = "节日抽奖池类型不能为空")
    private Integer poolType;

}
