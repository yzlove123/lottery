package com.example.lottery.module.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author liulei, lei.liu@htouhui.com
 * @version 1.0
 */
@Data
@Accessors(chain = true)
public class PrizePoolDto implements Serializable {
    private static final long serialVersionUID = 3049951515311931118L;
    /**
     * 主键
     */
    private Integer id;

    /**
     * 1-中秋抽奖池 2-国庆奖品池 3-元旦抽奖池 4-欢闹春节抽奖池
     */
    @NotNull(message = "节日抽奖池类型不能为空")
    private Integer poolType;

    /**
     * 奖品编号
     */
    private String prizeCode;

    /**
     * 1-钻石礼盒 2-铂金礼盒 3-黄金礼盒 4-热情礼盒
     */
    private Integer level;

    /**
     * 库存
     */
    private Integer inventory;

    /**
     * 抽中限制[单个用户]
     */
    private Integer once;

    /**
     * 每天的抽中次数限制[针对所有用户]
     */
    private Integer dayTimes;

    /**
     * N天内不允许再次抽中[针对所有用户]
     */
    private Integer limitDays;

    /**
     * 1-启用 0-停用
     */
    private Integer status;

    private BigDecimal rate;

    private Integer version;

}
